#!/bin/sh

/usr/bin/dockerd &> file & # start dockerd

while true
do
   if ps -aux | grep dockerd # check if dockerd started
      then
         sleep 2 
	 for image in /root/*.tar # load all images from volume mnt dir into container docker repo
         do
	    docker load < $image
	 done
	 
	 for image in $(docker image ls --format "{{.Repository}}:{{.Tag}}") # Start scan
         do
            echo "****************************** SCAN $image *********************************"		 
            trivy image -f json -o /reports/${image}.json ${image}

	    if docker run -i --rm --entrypoint=/bin/cat ${image} /etc/os-release | grep -i "redos"
	    then	     
               oscap-docker --disable-atomic image ${image} oval eval --result /reports/${image}.xml redos.xml
            elif docker run -i --rm --entrypoint=/bin/cat ${image} /etc/os-release | grep -i "astralinux"
            then
               oscap-docker --disable-atomic image ${image} oval eval --result /reports/${image}.xml AstraSE17VulnsOVAL.xml
	    fi
         done
         break
      else
         sleep 2 
         continue
   fi
done
