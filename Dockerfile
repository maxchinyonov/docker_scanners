FROM redos:7.3.2

WORKDIR /
RUN dnf update -y && \
dnf install -y docker-ce && \
dnf install -y openscap openscap-containers
COPY entrypoint.sh redos.xml AstraSE17VulnsOVAL.xml /
RUN chmod +x entrypoint.sh && \
mkdir /reports && \
dnf install -y python3-pip.noarch && \
pip3.8 install docker --quiet && \
dnf install -y libxslt --quiet && \
rpm -ivh https://github.com/aquasecurity/trivy/releases/download/v0.18.3/trivy_0.18.3_Linux-64bit.rpm
ENTRYPOINT ["./entrypoint.sh"]
