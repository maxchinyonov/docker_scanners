#!/bin/bash


download_test_images() {
docker pull nginx:latest
docker pull ubuntu:latest
wget https://vault.astralinux.ru/images/alse/docker/alse-slim-1.7.3uu1-docker-mg9.1.2.tar -O astra1.7.3uu1.tar
cat astra1.7.3uu1.tar | docker import - astra:1.7.3uu1 && rm -f astra1.7.3uu1.tar
}


# Get content and build redos image with trivy + openscap
get_content_and_build() {
wget https://share.red-soft.ru/index.php/s/NCeJkLeHQofkG9m/download -O redos.tar.gz
curl -O https://redos.red-soft.ru/support/secure/redos.xml
docker load < redos.tar.gz
docker build -t redos_scanners:v1 . -f Dockerfile
}


# Get all images from local repository
save_images_from_local_repo(){
for image in $(docker image ls --format "{{.Repository}}:{{.Tag}}")
do
   docker save ${image} > containers_images/${image}.tar
done
}


# Start scan
start_scan() {
docker run -v $PWD/containers:/var/lib/containers \
           -v $PWD/containers_images:/root        \
	   -v $PWD/reports:/reports               \
           -ti --privileged redos_scanners:v1
}


# Delete all files from mnt volumes
cleanup() {
	rm -rf $PWD/containers/*
	rm -rf $PWD/containers_images/*
}	


download_test_images
get_content_and_build
save_images_from_local_repo
start_scan
cleanup
